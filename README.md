## WGCLOUD-v2.1

服务器集群监控，ES集群监控，CPU监控，内存监控，DevOps运维工具，数据安全监控，性能指标数据可视化。

1.采用服务端和客户端协同工作方式，更轻量，更高效，可支持数百台服务器在线监控。

2.server端负责接受数据，处理数据，展示数据。

agent端负责上报服务器指标数据。

3.支持主流服务器平台Linux, Windows, Solaris, AIX, HP-UX等。

4.运行所需sigar的so，dll文件请到下载链接下载安装包，解压后在lib下。

5.采用微服务springboot+bootstrap实现

## **下载**

<http://www.wgstart.com/docs.html>

## **开源协议**

<http://www.wgstart.com/docs12.html>



![b.jpg](https://raw.githubusercontent.com/tianshiyeben/wgcloud/master/demo/demo2.jpg)

![c.jpg](https://raw.githubusercontent.com/tianshiyeben/wgcloud/master/demo/demo3.jpg)

![c.jpg](https://raw.githubusercontent.com/tianshiyeben/wgcloud/master/demo/demo4.jpg)

![c.jpg](https://raw.githubusercontent.com/tianshiyeben/wgcloud/master/demo/demo5.jpg)



## 运行环境

1.JDK1.8

3.mysql5.6 或 5.7



## 联系

http://www.wgstart.com/docs12.html

